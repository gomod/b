package b

import (
	"fmt"

	"gitlab.com/gomod/d"
)

func Say(l int) {
	fmt.Printf("%v%v\n", "                       "[:l*2], "B: 1.2.0")
	d.Say(l + 1)
}
